syntax on							" Enable syntax processing
set background=dark		" if dark theme
set t_Co=256					" Use 256 colors
colorscheme lettuce		" Color theme
" Cursorline appearance
:hi Normal ctermfg=darkcyan
:hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
:hi CursorColumn cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
:hi LineNr			 cterm=NONE ctermbg=darkred ctermfg=yellow guibg=darkred guifg=yellow
" Spell
:hi SpellBad cterm=underline,bold

" Spaces & Tabs
set tabstop=2					" # of visual spaces for TAB
set softtabstop=2			" # of spaces in TAB when editing
set shiftwidth=2			" # of spaces when autoindent (<<, >>;...)
set expandtab				  " Replace TAB with spaces
set breakindent
set showbreak=\\\\\

" UI Layout
filetype off
set number											" Show line numbers
set relativenumber							" Show relative line number to current line
set showcmd											" Show command in status line.
set cursorline									" Highlight cursor line
filetype plugin indent on				" Load filetype-specific indent files
set wildmenu										" Visual autocomplete for command menu
set wildmode=longest,list,full	" Visual autocomplete for command menu
set guifont=Liberation\ Mono\ 13	" Bigger font
set showmatch										" Highlight matching [{()}].
nnoremap gV `[v`]								" Highlight last inserted text
set nocompatible
set autowrite										" Autosave before commands like :next and :make
set hidden											" Hide abandoned buffers
set mouse=a											" Enable mouse usage (all modes)
set scrolloff=2									" Keep 2 lines between cursor and screen bottom
set autochdir										" Auto change pwd when buffer changes

" Searching
set incsearch															" Search as characters are entered
set hlsearch															" Highlight matches
set ignorecase														" Do case insensitive matching
set smartcase															" Do smart case matching

" Folding
set foldenable					" Enable folding
set foldmethod=indent		" Fold based on indent level
set foldlevelstart=10   " open most folds by default
"nnoremap <space> za			" space open/closes folds

" leader shortcuts
let mapleader=","													" leader is comma
let maplocalleader=","										" localleader is comma
nnoremap <leader>ev :vsp $MYVIMRC<CR>			" Edit .vimrc
nnoremap <leader>sv :source $MYVIMRC<CR>	" Source .vimrc
nnoremap <leader>gu :Utl<CR>											" Utl plugin
" nnoremap ,t :w\|!txt2tags -t html5 %<CR>	" Txt2tags running
" TODO : Inslide call (through Pandoc and reveal.js)
nnoremap <leader>c :set cursorline! cursorcolumn!<CR>	" Toggle cursor column visual indication
nnoremap <leader>h :nohlsearch<CR>	" turn off search highlight
"nnoremap <leader>p :!print2in1.sh <cfile><CR>	" Use my print2in1.sh script (on pdf files)
"nnoremap <leader>r :!multiGen %<CR>	" Use my multiGen.sh script on opened Markdown file
"nnoremap <leader>p :!multiGen <cfile><CR>	" Use my multiGen.sh script on md, pdf, png, jpg files
nnoremap <leader>r :!repeatDoc %<CR>	" Use my repeatDoc script (on current Markdown files)
nnoremap <leader>R :!repeatDoc <cfile><CR>	" Use my repeatDoc script (on <cfile> Markdown files)
nnoremap <leader>of :!openFile <cfile><CR>	" Use my openFile script
nmap <leader>b :Buffers<CR>
nmap <leader>f :Files<CR>
nmap <leader>g :Tags<CR>
nmap <F3> i<C-R>=strftime("%Y-%m-%d %H:%M")<CR><Esc>
imap <F3> <C-R>=strftime("%Y-%m-%d %H:%M")<CR>
nmap <F5> :setlocal spell! spelllang=fr<CR>
imap <F5> <C-o>:setlocal spell! spelllang=fr<CR>
" command W :execute ':silent w !sudo tee % > /dev/null' | :edit!
nmap <leader><leader>1 :set foldlevel=0<CR>
nmap <leader><leader>2 :set foldlevel=1<CR>
nmap <leader><leader>3 :set foldlevel=2<CR>
nmap <leader><leader>4 :set foldlevel=3<CR>
nmap <leader><leader>5 :set foldlevel=4<CR>
nmap <leader><leader>6 :set foldlevel=5<CR>
nmap <leader><leader>7 :set foldlevel=6<CR>

" Markdown headings
nnoremap <leader>1 m`yypVr=``
nnoremap <leader>2 m`yypVr-``
nnoremap <leader>3 m`^i### <esc>``4l
nnoremap <leader>4 m`^i#### <esc>``5l
nnoremap <leader>5 m`^i##### <esc>``6l
" Moving lines
nnoremap <silent> <C-j> :move+1<cr>
nnoremap <silent> <C-k> :move-2<cr>
nnoremap <silent> <C-h> <<
nnoremap <silent> <C-l> >>
xnoremap <silent> <C-j> :move+1<cr>gv
xnoremap <silent> <C-k> :move-2<cr>gv
xnoremap <silent> <C-h> <gv
xnoremap <silent> <C-l> >gv
xnoremap < <gv
xnoremap > >gv

" Keymappings
inoremap jk <Esc>													" jk is ESC in insert mode
" Arrows to move text
nmap <Left> <<
nmap <Right> >>
vmap <Left> <gv
vmap <Right> >gv
nmap <Up> [e
nmap <Down> ]e
vmap <Up> [egv
vmap <Down> ]egv
nnoremap <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
nnoremap <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'

" Remap F1 to ESC
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
nnoremap <F12> :TagbarToggle<CR>
" Lazylist plugin
nnoremap gll :LazyList<CR>
vnoremap gll :LazyList<CR>
nnoremap gl- :LazyList '- '<CR>
nnoremap gl- :LazyList '- '<CR>
nnoremap gl* :LazyList '* '<CR>
nnoremap gl* :LazyList '* '<CR>
" nnoremap <Esc><Esc> :w<CR>					" Save when you hit Esc twice
nnoremap <space> i<Space><Esc>			" Insert a space staying in normal mode
nnoremap <S-CR> O<ESC>				" Insert new line above current staying in normal mode
nnoremap <CR> o<ESC>				" Insert new line below staying in normal mode
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
" sudo save
cmap w!! w !sudo tee % >/dev/null
" nnoremap H gT
" nnoremap L gt
map f <Plug>Sneak_f
map F <Plug>Sneak_F
map t <Plug>Sneak_t
map T <Plug>Sneak_T
let g:sneak#label = 1

map <F4> :GitGutterToggle<CR>

" CtrlP
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
" let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

" Call Pathogen
call pathogen#infect()
call pathogen#runtime_append_all_bundles()

" Autogroups
augroup configgroup
    autocmd!
    autocmd VimEnter * call after_object#enable('=', ':', '-', '#', ' ')
    autocmd VimEnter * highlight clear SignColumn
    " autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md
    "									\:call <SID>StripTrailingWhitespaces()
    autocmd FileType vimwiki setlocal nospell
    autocmd FileType java setlocal noexpandtab
    autocmd FileType java setlocal list
    autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
    autocmd FileType php setlocal expandtab
    autocmd FileType php setlocal list
    autocmd FileType php setlocal listchars=tab:+\ ,eol:-
    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
    autocmd FileType mail execute 'hi Normal ctermfg=white'
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
    autocmd BufNewFile *.t2t 0r ~/.vim/templates/t2t.t2t " txt2tags template
    autocmd BufNewFile,BufRead *.t2t set ft=txt2tags " txt2tags syntax highlighting
    autocmd BufNewFile,BufRead *.todo set ft=todo " todo syntax highlighting
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufEnter *.sh setlocal tabstop=2
    autocmd BufEnter *.sh setlocal shiftwidth=2
    autocmd BufEnter *.sh setlocal softtabstop=2
    " Jump to the last position when reopening a file
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
augroup END

" Keep all folds open when a file is opened
" augroup OpenAllFoldsOnFileOpen
"     autocmd!
"     autocmd BufRead * normal zR
" augroup END


let g:tex_flavor='latex'

" Vimwiki
let wiki_default = {}
let wiki_default.auto_export = 0
let wiki_default.syntax = 'markdown'
let wiki_default.ext = '.md'
let wiki_default.auto_toc = 0
let wiki_default.nested_syntaxes = {'python': 'python', 'c++': 'cpp', 'sh': 'sh'}
let g:vimwiki_folding = 'expr'
let g:vimwiki_hl_headers = 1
:hi VimwikiHeader1 guifg=#FF0000 ctermfg=blue
:hi VimwikiHeader2 guifg=#00FF00 ctermfg=yellow
:hi VimwikiHeader3 guifg=#0000FF ctermfg=white
:hi VimwikiHeader4 guifg=#FF00FF ctermfg=gray
:hi VimwikiHeader5 guifg=#00FFFF ctermfg=darkgreen
:hi VimwikiHeader6 guifg=#FFFF00 ctermfg=green

let home_wiki = copy(wiki_default)
let home_wiki.path = '~/vimwiki/'
let home_wiki_path_html = '~/vimwiki/html/'
let home_wiki.diary_rel_path = 'diary/'

let work_wiki = copy(wiki_default)
let work_wiki.path = '~/college/cours/'
let work_wiki.diary_rel_path = 'calendar/'

let g:vimwiki_list = [work_wiki, home_wiki]

" Airline
set laststatus=2		" statusline always on 
let g:airline#extensions#branch#enabled = 1

" Todo.txt Vim plugin
let g:todo_root='~/Cozy\ Drive/todo'
" Use todo#Complete as the omni complete function for todo files
au filetype todo setlocal omnifunc=todo#Complete


" Omnifunc
set omnifunc=syntaxcomplete#Complete

" Vim-reveal
let g:reveal_root_path = '/home/celfred/college/reveal.js/'

" Check attachments in mails
let g:attach_check_keywords =',joint,joints,jointe,jointes'
let g:checkattach_filebrowser = 'ranger'

" Custom functions
set autoread

"augroup autoSaveAndRead
"    autocmd!
"    autocmd TextChanged,InsertLeave,FocusLost * silent! wall
"    autocmd CursorHold * silent! checktime
"augroup END

" Toggle between number and relativenumber
function! ToggleNumber()
    if(&relativenumber == 1)
        set norelativenumber
        set number
    else
        set relativenumber
    endif
endfunc

function! VimwikiLinkHandler(link)
  " Use Vim to open external files with the 'vfile:' scheme.  E.g.:
  "   1) [[vfile:~/Code/PythonProject/abc123.py]]
  "   2) [[vfile:./|Wiki Home]]
  let link = a:link
  if link =~# '^vfile:'
    let link = link[1:]
  else
    return 0
  endif
  let link_infos = vimwiki#base#resolve_link(link)
  if link_infos.filename == ''
    echom 'Vimwiki Error: Unable to resolve link!'
    return 0
  else
    exe 'tabnew ' . fnameescape(link_infos.filename)
    return 1
  endif
endfunction

" Strips trailing whitespace at the end of files. this
" is called on buffer write in the autogroups section.
function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction
" }}}

set modelines=1 " .vimrc special settings 
" vim:foldmethod=marker:foldlevel=0

set rtp+=~/.fzf

map <leader>G :Goyo <CR>

" autocomplétion emojis avec Ctrl-E
let g:emoji_complete_overwrite_standard_keymaps = 0
imap <C-E> <Plug>(emoji-start-complete)

function! VO2MD()
  let lines = []
  let was_body = 0
  for line in getline(1,'$')
    if line =~ '^\t*[^:\t]'
      let indent_level = len(matchstr(line, '^\t*'))
      if was_body " <= remove this line to have body lines separated
        " call add(lines, '')
      endif " <= remove this line to have body lines separated
      call add(lines, substitute(line, '^\(\t*\)\([^:\t].*\)', '\=repeat("#", indent_level + 1)." ".submatch(2)', ''))
      " call add(lines, '')
      let was_body = 0
    else
      " call add(lines, substitute(line, '^\t*: ', '', ''))
      call add(lines, line)
      let was_body = 1
    endif
  endfor
  silent %d _
  call setline(1, lines)
endfunction

function! MD2VO()
  let lines = []
  for line in getline(1,'$')
    if line =~ '^\s*$'
      continue
    endif
    if line =~ '^#\+'
      let indent_level = len(matchstr(line, '^#\+')) - 1
      call add(lines, substitute(line, '^#\(#*\) ', repeat("\<Tab>", indent_level), ''))
    else
      call add(lines, substitute(line, '^', repeat("\<Tab>", indent_level) . ': ', ''))
    endif
  endfor
  silent %d _
  call setline(1, lines)
endfunction
