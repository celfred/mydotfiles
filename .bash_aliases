alias coll="cd ~/college/"
alias cours="cd ~/college/cours/"
alias boulot="vim -S ~/college/cours/Session.vim"
alias inslide="cd ~/college/inslide/"
alias planetalert="cd ~/PlanetAlert/site/templates/"
alias jinvite="cd ~/jinvite-last/site/templates/"
alias flgames="cd ~/FLGames/"

alias t="todo.sh -n"
alias tp="todo.sh ls +PlanetAlert"
alias tt="todo.sh ls +todo"
alias tw="todo.sh ls +work"

alias mycal="rem -cl+2 -m -w80,10,1 -b1" # Display remind calendar for 2 weeks
alias er="vim ~/.reminders" #Edit reminders file
alias cal="cal -A1" # Display simple calendar for current and next months

alias mutt="cd ~/Bureau && mutt"

#alias t='task'
#alias tm='task pro:maison'
#alias tb='task pro:boulot'
#alias to='task pro:ordi'
#alias tu='task pro:url minimal rc.hyphenate:off'
#alias tf='task fred'

alias go='gnome-open '
alias o='xdg-open '

alias help='tldr'

alias cpimage='xclip -selection clipboard -type image/png'
